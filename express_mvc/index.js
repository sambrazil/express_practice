const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const pool = require('./lib/db')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: false,
    viewExt: 'ejs',
  });

 const userModel = require('./models/user')
 const userTemp = require('./controllers/user')
 const userController = userTemp(userModel, pool) 

  router.get('/show_by_id/:id',userController.searchById)

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)