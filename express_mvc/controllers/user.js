module.exports = function (userModel, pool){
    return{
        async searchById (ctx, next){
            
            const obj = await userModel.showById(pool, ctx.params.id)
            if(obj.error){
                ctx.status = 400
                ctx.body = obj.error
                return
            }
          ctx.body = obj
        }
    }
}