const express = require('express')
const mysql2 = require('mysql2/promise')
const layout = require('express-ejs-layouts')
const path = require('path')

const app = express()
app.use(layout)
//set ejs
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.set('layout', 'template')
//set db
const pool = mysql2.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'codecamp3_project'
})

app.get('/', async( req, res, next)=>{
   const [rows, fields] = await pool.query(`SELECT * FROM user WHERE email =?`,['1@1'])
   res.render('data',{user:rows})
})

app.listen(3000)