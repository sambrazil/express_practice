const userModel = require('../models/user')
const express = require('express')
const router =  express.Router()
// router คือ middleware ถ้าเราเข้าที่ path  จะทำงาน ตามลำดับ ปล. อย่าลืม next()
router.post('/search',async(req,res, next)=>{
  console.log('Middleware 1 working')
   
  const {name} = req.body || null
  const findCondition = {name:name}
   const findUser = await userModel.find(findCondition) 
 
   if(!findUser[0]) return res.send('no found name') 
  
  req.user = findUser
   next()
})

router.post('/search', (req, res, next)=>{
    console.log('Middleware 2 working')
    res.send(req.user)
})

router.get('/search_by_name/:name', (req, res, next)=>{
  const {name} = req.params || null
 res.send('name : '+name )
})

module.exports = router