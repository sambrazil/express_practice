const mongoose = require('mongoose')
const userSchema = mongoose.Schema({
    name: {type:String},
    age: {type:Number, required:true}
})
const userModel = mongoose.model('users',userSchema)

module.exports = userModel