const express = require('express')
const path = require('path')
const db = require('./db')
const searchRouter = require('./routes/search')

const app = express()
app.use(express.json())
app.use(express.urlencoded({extended: true}))


app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

db.on('error', (err)=>{
    console.error(err)
})
db.once('open',()=>{
    console.log('Connect successfully')
})

app.get('/search', (req,res)=>{
    res.render('search')
})
app.post('/search', searchRouter)

app.get('/search_by_name/:name',searchRouter)

app.listen(3000)