const bcrypt = require('bcrypt')
module.exports = function(userModel,pool){
    return{
        getLogin(req, res, next){ 
            res.render('loginForm')
             next()
        },
        async postLogin(req, res, next){
            const email = req.body.email
            const password = req.body.password
            const obj = await userModel.checkLogin(email, pool)
            if(obj.error){
                res.status(obj.status)
                req.flash('error',obj.error)
                res.redirect('/')
                return
            }
            const comparePassword = await bcrypt.compare(password, obj[0].password)
            if(comparePassword){
                
               req.flash('success','login complete')
               req.session.userId = obj[0].id
               res.redirect('/profile')
               return
            }
            req.flash('error', 'wrong password')
            res.redirect('/')
            
        },
           getRegister(req, res, next){
             res.render('register')  
           },
        async postRegister(req, res, next){
            const email = req.body.email
            const password = req.body.password
            const confirmPassword = req.body.confirmPassword
            const hashPassword = await bcrypt.hash(password, 10)
            if(password !== confirmPassword){
               req.flash('error','password not match')
               res.redirect('/register')
               return
            }
           const checkEmail = await userModel.checkExitEmail(email, pool)
           if(checkEmail.error){
               req.flash('error',checkEmail.error)
               res.redirect('/register')
               return
           }
           const insertData = await userModel.registerUser(email, hashPassword, pool)
           if(insertData.insertId){
            req.flash('success', insertData.success)
               res.redirect('/register')
           }      
        },
         async getProfile(req, res, next){
             const data = await userModel.getProfile(req.session.userId, pool)
             res.render('profile',{data:data})
             next()
         }

    }
}