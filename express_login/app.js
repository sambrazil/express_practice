const express = require('express')
const pool = require('./db')
const layout = require('express-ejs-layouts')
const path =require('path')
const flash = require('express-flash-messages')
const session = require('express-session')

const app = express()

const checkSession = function(req, res, next){
    
        if(!req.session || !req.session.userId ){
            res.redirect('/')
        }
       else{
        next()
        }
    }

const haveSession = function(req, res, next){
    if(!req.session || !req.session.userId ){
        next()
    }
   else{
      res.redirect('/profile')
    }
}
// expires:false ลบ session เมื่อปิด browser
app.use(session({cookie:{maxAge: 3600000,expires:false}, saveUninitialized:false, resave:false, secret:'*/74adssaA'}))
app.use(flash())
app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.use(layout)

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.set('layout', 'template')

const userTemp = require('./controllers/user')
const userModel = require('./models/user')
const userController = userTemp(userModel, pool)


app.get('/',haveSession,userController.getLogin)
app.post('/',userController.postLogin)
app.get('/register',userController.getRegister)
app.post('/register', userController.postRegister)
app.get('/profile', checkSession,userController.getProfile)

app.listen(3000)