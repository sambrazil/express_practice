module.exports = {
    async checkLogin (email,pool){ 
        const [rows] = await pool.query('SELECT id, email, password FROM user WHERE email =?',[email])
         if(!rows[0]){
      
             return {error:'wrong email', status:400}
          
         }
         return rows
    },
    async registerUser(email, password, pool){
      const [rows] = await pool.query('INSERT INTO user (email, password) VALUES (?,?)',[email, password])
        return {insertId: rows.insertId, success:'register success'}
     
    },
    async checkExitEmail(email,pool){
        const [rows] = await pool.query('SELECT id FROM user WHERE email =?',[email])
        if(!rows[0]){
            return {success:'can use this email to register'}
        }
        return {error:'this email already exit'}
    },
    async getProfile(userId, pool){
        const [rows] = await pool.query('SELECT email, username, firstname, lastname FROM user WHERE id =?',[userId])
        if(!rows[0]){
            return {error:'no found'}
        }
        return rows
    }

}