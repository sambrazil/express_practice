require('dotenv').config()
const express = require('express')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const pool = require('./db')
const cors = require('cors')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use(cors({orgin:'*',
              methods: ['GET','POST','DELETE'],
              allowedHeaders:['Content-Type', 'Authorization']}))
 // gernerate new access token
function generateToken (payload){
    const token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {expiresIn:60*10})
    return token
} 

// verify token middle ware
// header format 
// Authorization : Bearer token
 function verifyToken(req, res, next){
     const authHeader = req.headers['authorization'] || null
     const token =  authHeader && authHeader.split(' ')[1]
     if(!authHeader) return res.status(400).send('not found authHeader')
     if(!token) return res.status(400).send('not found token')

     jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data)=>{
         if(err) return res.status(400).send(err)
         req.data = data  //ส่งข้อมูลไปยัง function ถัดไป , ใน Function ถัดไป ให้ req.data เพื่อเรียกข้อมูล
         next() // ไปยัง function ถัดไป
     })
 }


//login
app.post('/login',async(req,res, next)=>{
    const email = req.body.email || null // logic operator ถ้าไม่่มีค่าให้เป็น null
    const password =  req.body.password || null
      if(!email) return res.status(400).send('please input email')
      if(!password) return res.status(400).send('please input password')
    const [checkEmail] = await pool.query('SELECT * FROM user WHERE email =?',[email])
    if(!checkEmail[0]) return res.status(400).send('wrong email')

    const comparePassword = await bcrypt.compare(password, checkEmail[0].password)
    if(!comparePassword) return res.status(400).send('wrong password')

    const payload = { user_id: checkEmail[0].id,
                      email: checkEmail[0].email,
                      firstname: checkEmail[0].firstname,
                      lastname: checkEmail[0].lastname
                    }

   const accessToken  = await generateToken(payload) || null
   const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET) || null

   if(!accessToken || !refreshToken) return res.status(404).send('can not sign token')

   const [checkRefreshToken] = await pool.query('SELECT * FROM token WHERE user_id =?',payload.user_id)
    if(!checkRefreshToken[0]){
        const [insertRefreshToken] = await pool.query(` INSERT INTO token (refresh_token, user_id) VALUES (?,?)`
                                                        ,[refreshToken, payload.user_id])
    } 
    else{
       const [updateRefreshToken] = await pool.query(` UPDATE token SET refresh_token =? WHERE user_id =? `
                                                       ,[refreshToken, payload.user_id])
    }
    res.json({ accessToken: accessToken,
               refreshToken: refreshToken
            })
})  

//show profile fontend send acessToken to backend
// Authorization : Bearer token
app.post('/profile',verifyToken, (req, res, next)=>{
    const {user_id, email, firstname, lastname} = req.data || null
    res.send(` Hi ${firstname} ${lastname}\n id: ${user_id}\n email: ${email}`)
})

// send refreshToken to generate new acessToken
app.post('/token', async(req, res, next)=>{
    const authHeader = req.headers['authorization'] || null
    const token = authHeader && authHeader.split(' ')[1]

    if(!authHeader) return res.status(400).send('not found authHeader')
    if(!token) return res.status(400).send('not found refreshToken')

    const [checkRefreshToken] = await pool.query('SELECT * FROM token WHERE refresh_token =?',[token])
    if(!checkRefreshToken[0]) return res.status(400).send('invalid refreshToken')
    jwt.verify(token, process.env.REFRESH_TOKEN_SECRET, async(err,data)=>{
        if(err) return res.status(400).send(err)
        const newAccessToken = await generateToken(data)
        if(!newAccessToken) return res.status(400).send('not found newAccessToken')
        res.json({ accessToken : newAccessToken  })
    })
})

// delete refreshToken in database
app.delete('/delete_refresh_token', async(req,res, next)=>{
    const authHeader = req.headers['authorization'] || null
    const token = authHeader && authHeader.split(' ')[1]
    if(!authHeader) return res.status(400).send('not found authHeader')
    if(!token) return res.status(400).send('not found token')

    const [checkRefreshToken] = await pool.query('SELECT * FROM token WHERE refresh_token =?',[token])
    if(!checkRefreshToken[0]) return res.status(400).send(' invaild token')
    const [deleteRefreshToken] = await pool.query('DELETE FROM token WHERE refresh_token =?',[token])
    res.send('delete refresh token done')
})

app.listen(3000)