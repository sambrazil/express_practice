# วิธีใช้ docker กับ nodejs
## 1. สร้าง Dockerfile
  FROM node:9-slim   >> ใช้node version 9-slim


WORKDIR /app  >> directory ที่ทำงานบน docker

COPY package.json /app 
>> copy file package.json

RUN npm install   
>> ให้ npm install เพื่อติดตั้ง package ทั้งหมด

COPY . /app  
>> copy file project ทั้งหมด

CMD ["npm","start"] 
>> ให้  npm start

## 2. สร้าง .dockerignore

>> เพื่อไฟล์ที่เราไม่ต้องการออกไป เช่น

>>node_modules

>>npm-debug.log

>>package-lock.json

## 3. เปิด terminal ที่ directory ของ project ในเครื่องเรา

### 3.1 docker build -t node-docker-tutorial .

>> สร้าง docker image

>> (node-docker-tutorial คือ ชื่อ image จะเปลี่ยนเป็นชื่ออะไรก็ได้แล้ว )


### 3.2 docker run -it -d --name express -p 80:3000 image_name

>> สร้าง docker container

>> -p = port ให้เข้าที่ip  http://192.168.99.100:80

>> --name = ชื่อ container

>> -d = ให้ container เป็น background

