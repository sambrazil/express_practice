const express = require('express')
const uuid = require('uuid/v4')
const path = require('path')
const layout = require('express-ejs-layouts')
const multer = require('multer')
const flash = require('express-flash-messages')
const session = require('express-session')

const app = express()
app.use(session({ cookie:{maxAge:60000, expires: false}, saveUninitialized:false, resave:false, secret:'@!$%^&*qweAs' })) 
app.use(flash())
app.use(express.json())
app.use(express.urlencoded({extended:false}))
//setting ejs
app.use(layout)
app.set('views', path.join(__dirname,'views'))
app.set('view engine', 'ejs')
app.set('layout','template')

// กำหนด path ที่เก็บไฟล์ และตั้งชื่อไฟล์ใหม่
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/images')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now()+'-'+ uuid()+'-'+file.originalname)
  }
})
// ให้ upload ไฟล์รูปภาพ ถ้าไม่ใช่จะ return error
const upload = multer({ storage: storage,
                       fileFilter: function(req, file, cb){
                         let extension = path.extname(file.originalname)
                         if(extension !=='.jpeg' && extension !=='.jpg' && extension !=='.png' && extension !== '.gif'
                         && extension !=='.JPEG' && extension !=='.JPG' && extension !=='.PNG' && extension !=='.gif' )
                         {
                             return cb(new Error('Only image are allow'))
                         } 
                         cb(null, true)
                       } 
                    })
  // middleware ใช้ดัก error แล้ว set ค่า flash message กับ redirect ไปยังหน้าที่ต้องการ                  
const handleUploadError = function(err, req, res, next){
      const errString = err.toString() // แปลงค่า error ให้เป็น string ถ้าไม่แปลงจะเอาค่าไปโชว์ใน res.send ,req.flash ไม่ได้
      const errLength = errString.length
      const errSubstring = errString.substr(7, errLength) //ตัดเอาคำว่า Error:  ออก
      //ดัก error
      if(err){
         req.flash('error',errSubstring)
         res.redirect('/upload')
         return
      } 
      next()
}                      
                  
app.get('/upload', (req, res) =>{
    res.render('upload')
})
app.post('/upload',upload.fields([{name:'picture'},{name: 'picture2'}]),handleUploadError ,(req, res,next) =>{  
   const picture = req.files.picture
   const picture2 = req.files.picture2
    // ถ้าไม่ได้ใส่รูปมา
  if(!picture && !picture2){
        req.flash('error', 'please upload file')
        res.redirect('/upload')
        return
      }
     req.flash('success', 'upload success')
     res.redirect('/upload')
     next()
})

app.use(express.static(path.join(__dirname,'public')))
app.listen(3000)
