const bcrypt = require('bcrypt')
module.exports = function(userModel, pool){
  return{
     async login(req, res, next){
       const email = req.body.email
       const password = req.body.password
       const checkEmail = await userModel.checkEmailLogin(email,pool)
       if(checkEmail.error){
         res.status(checkEmail.status).send(checkEmail.error)
         return
       }
       const comparePassword = await bcrypt.compare(password, checkEmail[0].password)
        if(comparePassword){
          res.send({success: 'login complete'})
          return
        }
         res.status(400).send('wrong password') 
     },
     async upload(req, res, next){
       const picture = req.files.picture
         if (!picture){
           res.status(400).send('plase input picture')
           return
         }
        const pictureName = req.files.picture[0].filename
        console.log(pictureName)
        const picturePath = '/'+ pictureName
         const upload = await userModel.upload(picturePath, pool)
         if(upload){
           res.send('upload complete')
           return
         }
         res.send('upload fail')
     }
    
  }

}