module.exports ={
   async upload (picture, pool){
       const [row] = await pool.query(`INSERT INTO user_profile_picture (profile_picture_url) VALUES
                                                                      (?)`,[picture])
        return row.insertId                                                               
   },
  async checkEmailLogin(email, pool){
     const [row] = await pool.query('SELECT * FROM user WHERE email =?',[email])
     if(!row[0]){
        return {error: 'wrong email', status:'400'}
     }
     return row
     
  } 
}