const uuid = require('uuid/v4')
const multer = require('multer')
const path =require('path')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'images')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + '-' + uuid()+'-'+file.originalname)
    }
  })
  
  const upload = multer({ storage: storage,
                          fileFilter: function(req, file, cb){
                              const extension = path.extname(file.originalname)
                              if(extension !== '.jpg' && extension !== '.jpeg' && extension!=='.png' && extension!=='.gif'
                                 && extension !== '.JPG' && extension !== '.JPEG' && extension!=='.PNG' && extension!=='.GIF')
                                 {
                                        return cb(new Error('Only allow picture'))
                                 }
                                 cb(null, true)
                          }})

const handleError = function (err, req, res, next){
    const stringErr = err.toString()
    const lengthErr = err.length
    const substrErr = stringErr.substr(0, lengthErr)
    if(err){
        res.status(500).send(substrErr)
        console.log(err)
        return
    }
    next()
}

module.exports = {upload, handleError}