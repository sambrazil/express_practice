const express = require('express')
const pool = require('./libs/db')
const cors = require('cors')
const path = require('path')
const app = express()
var whitelist = ['http://localhost:4000']
var corsOptions = {
  /*
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
  */
    origin :'*',
  methods: ['POST', 'GET'],
  allowedHeaders: ['Content-Type',]
}
app.use(cors(corsOptions))
app.use(express.static(path.join(__dirname, 'images')))
app.use(express.json())
app.use(express.urlencoded({extended:false}))

const multer = require('./settings/upload')
const handleError = multer.handleError
const upload = multer.upload

const userModel = require('./models/user')
const userTemp = require('./controllers/user')
const userController = userTemp(userModel, pool)

app.post('/login',userController.login)
app.post('/upload', upload.fields([{name:'picture', maxCount:1}]), handleError, userController.upload)
app.get('/', (req, res, next)=>{
     res.send('Hello World')
})
app.listen(3000)