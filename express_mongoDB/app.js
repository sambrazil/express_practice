const express = require('express')
const layout = require('express-ejs-layouts')
const path = require('path')
const db = require('./db')
const userModel = require('./models/user')
const app = express()
app.use(layout)
app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.set('layout', 'template')

   db.on('error',(err)=>{
       console.log(err)
   })
   db.once('open',()=>{
      console.log('Connect Successfully')
   })


app.get('/insert', (req, res)=>{
      res.render('insert')
})
app.post('/insert', async(req, res)=>{
    const {name, age} = req.body || null
    const insertData = {name : name, age : age}
    const userInstance = new userModel(insertData)
    await userInstance.save( err =>{
       if(err)  return res.send(err)
    })
    res.send('insert successfully')
})


app.get('/update', (req,res)=>{
   res.render('update')
})
app.post('/update', async(req,res)=>{
  
   const {name, age} = req.body || null
   const updateCondition = {name:name}
   const updateSet = { $set:{ age:age } }
   const findUser = await userModel.find(updateCondition, (err,data)=>{
       if(err) return res.send(err)    
   })    
   if(!findUser[0]) return res.send('not found name')
     
   const updateUser = await userModel.updateMany(updateCondition, updateSet)
   res.send('update complete')
})

app.get('/delete',async(req,res)=>{
   res.render('delete')
})
app.post('/delete', async(req, res)=>{
   const name = req.body.name || null
   const deleteCondition = {name:name}
 const findUser = await userModel.find(deleteCondition, (err,data)=>{
     if(err) return res.send(err)
 })
 if(!findUser[0]) return res.send('not found name')

const deleteUser = await userModel.deleteMany(deleteCondition, (err,data)=>{
   if(err) return res.send(err)
})
 res.send('delete complete')
})

app.get('/search', (req,res)=>{
   res.render('search')
})
app.post('/search', async(req,res)=>{
  const name = req.body.name || null
  const searchCondition = {name:name}
  const findUser = await userModel.find(searchCondition,{_id:0,name:1, age:1}, (err,data)=>{
     // {_id:0,name:1, age:1} >> 0 คือเอา field นั้นออก ,1 คือเลือก fieldนั้น >> เหมือน select age, name ใน mysql
     if(err) return res.send(err)

  })
  if(!findUser[0]) return res.send('not found name')
  res.send(findUser)
})


app.listen(3000)